#!/bin/bash
# Based on https://trac.ffmpeg.org/wiki/CompilationGuide/Centos

# Remove ffmpeg-related packages

yum remove ffmpeg SDL alsa-lib celt enca ffmpeg-libs flac fontconfig fribidi gsm lame-libs libICE libSM libXdamage libXext libXfixes libXi libXtst libXxf86vm libass libasyncns libcdio libdc1394 libogg liboil libraw1394 librtmp libsndfile libtheora libusb1 libv4l libva libvorbis mesa-dri-drivers mesa-dri-filesystem mesa-dri1-drivers mesa-libGL mesa-private-llvm openal-soft openjpeg-libs pulseaudio-libs schroedinger speex x264-libs xvidcore

# Compile ffmpeg from sources, as explained in https://trac.ffmpeg.org/wiki/CompilationGuide/Centos.

yum install autoconf automake gcc gcc-c++ git libtool make nasm pkgconfig zlib-devel

mkdir ffmpeg-sources

cd ~/ffmpeg-sources
git clone --depth 1 git://github.com/yasm/yasm.git
cd yasm
autoreconf -fiv
./configure --prefix="/usr/local/ffmpeg" --bindir="/usr/local/bin"
make
make install
make distclean

cd ~/ffmpeg-sources
git clone --depth 1 git://git.videolan.org/x264
cd x264
./configure --prefix="/usr/local/ffmpeg" --bindir="/usr/local/bin" --enable-static
make
make install
make distclean

cd ~/ffmpeg-sources
git clone --depth 1 git://git.code.sf.net/p/opencore-amr/fdk-aac
cd fdk-aac
autoreconf -fiv
./configure --prefix="/usr/local/ffmpeg" --disable-shared
make
make install
make distclean

cd ~/ffmpeg-sources
curl -L -O http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz
tar xzvf lame-3.99.5.tar.gz
cd lame-3.99.5
./configure --prefix="/usr/local/ffmpeg" --bindir="/usr/local/bin" --disable-shared --enable-nasm
make
make install
make distclean

cd ~/ffmpeg-sources
git clone git://git.opus-codec.org/opus.git
cd opus
autoreconf -fiv
./configure --prefix="/usr/local/ffmpeg" --disable-shared
make
make install
make distclean

cd ~/ffmpeg-sources
curl -O http://downloads.xiph.org/releases/ogg/libogg-1.3.2.tar.gz
tar xzvf libogg-1.3.2.tar.gz
cd libogg-1.3.2
./configure --prefix="/usr/local/ffmpeg" --disable-shared
make
make install
make distclean

cd ~/ffmpeg-sources
curl -O http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.4.tar.gz
tar xzvf libvorbis-1.3.4.tar.gz
cd libvorbis-1.3.4
./configure --prefix="/usr/local/ffmpeg" --with-ogg="/usr/local/ffmpeg" --disable-shared
make
make install
make distclean

cd ~/ffmpeg-sources
git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git
cd libvpx
./configure --prefix="/usr/local/ffmpeg" --disable-examples
make
make install
make clean

cd ~/ffmpeg-sources
curl -O http://downloads.xiph.org/releases/theora/libtheora-1.1.1.tar.gz
tar xzvf libtheora-1.1.1.tar.gz
cd libtheora-1.1.1
./configure --prefix="/usr/local/ffmpeg" --with-ogg="/usr/local/ffmpeg" --disable-examples --disable-shared --disable-sdltest --disable-vorbistest
make
make install
make distclean

cd ~/ffmpeg-sources
git clone --depth 1 git://source.ffmpeg.org/ffmpeg
cd ffmpeg
PKG_CONFIG_PATH="/usr/local/ffmpeg/lib/pkgconfig" ./configure --prefix="/usr/local/ffmpeg" --extra-cflags="-I/usr/local/ffmpeg/include" --extra-ldflags="-L/usr/local/ffmpeg/lib" --bindir="/usr/local/bin" --enable-gpl --enable-nonfree --enable-libfdk_aac --enable-libmp3lame --enable-libopus --enable-libvorbis --enable-libvpx --enable-libx264 --enable-libtheora
make
make install
make distclean
hash -r